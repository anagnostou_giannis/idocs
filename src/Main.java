import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Ioannis Anagnostou on 05-Nov-16.
 * anagnostou.janos@gmail.com
 */
public class Main {

    public static void main(String[] args) {
        if(args.length!=3){
            System.out.println("Arguments missing..");
            return;
        }

        Set<String> set1 = null;
        Set<String> set2 = null;
        try {
            set1 = Utils.readFile(args[0]);
            set2 = Utils.readFile(args[1]);
        } catch (IOException e) {
            System.out.println("A file could not be read..");
            e.printStackTrace();
            return;
        }

        set1.retainAll(set2);
        Set<String> sortedResult = new TreeSet<>(set1);
        try {
            Utils.writeFile(sortedResult, args[2]);
        } catch (IOException e) {
            System.out.println("Something went wrong while writing in file..");
            e.printStackTrace();
            return;
        }
    }
}
