I think this program is efficient because:

I used Java HashSets for not keeping duplicate strings and for O(1) contains performance.
I also used TreeSets for ordering for the result file.