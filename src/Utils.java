import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Ioannis Anagnostou on 05-Nov-16.
 * anagnostou.janos@gmail.com
 */
public class Utils {

    static Set<String> readFile(String filePath) throws IOException {
        FileInputStream inputStream = null;
        Scanner scanner = null;
        Set<String> set = new HashSet<>();
        try {
            inputStream = new FileInputStream(filePath);
            scanner = new Scanner(inputStream, "UTF-8");
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                set.add(line);
            }
            if (scanner.ioException() != null) {
                throw scanner.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (scanner != null) {
                scanner.close();
            }
        }
        return set;
    }

    static void writeFile(Set<String> resultSet, String filePath) throws IOException {
        FileOutputStream outputStream = null;
        PrintStream printStream = null;
        try {
            outputStream = new FileOutputStream(filePath);
            printStream = new PrintStream(outputStream);
            for(String line:resultSet) {
                printStream.print(line);
                printStream.println();
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (printStream != null) {
                printStream.close();
            }
        }
    }
}
